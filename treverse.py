#!/usr/bin/python3
import socket
import sys
input_parameter = sys.argv[1]
host_parameter = sys.argv[2]

if input_parameter == 's':
    
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(("", 5821))
    server.listen()
    print("Server is listening")
    
    while True:
        user_socket, adress = server.accept()

        print(f"User {user_socket} connected!")
        
        user_socket.send("You are connected".encode("utf-8"))    
        data = user_socket.recv(2048)
        print(data.decode("utf-8"))
    
elif input_parameter == 'c':
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    client.connect((socket.gethostbyname(host_parameter), 5821))

    while True:
        data = client.recv(2048)
        print(data.decode("utf-8"))

        client.send(input("message-: ").encode("utf-8")[::-1])
        
